---
layout: page
permalink: /
---

# COVID-19: Software Educativo Livre para Ensino e Teletrabalho

Num contexto em que as atuais circunstâncias relacionadas com o COVID-19 obrigam alunos, professores e trabalhadores a ficar em casa, a [ANSOL](https://ansol.org) apresenta uma lista de [software livre](https://ansol.org/filosofia) que pode ser útil à comunidade.

Estes programas são livres e gratuitos, e a maior parte está disponível para diversos sistemas operativos.


**Ajude a melhorar esta lista. Envie as suas sugestões através [deste repositório](https://gitlab.com/ubuntu-pt/open-edu/).**

## A ser instalado pelas instituições ou centralmente pelo Estado

### Plataformas de gestão de ensino digital (LMS)

* [Atutor LMS](https://atutor.github.io/)
* [Canvas LMS](https://www.instructure.com/canvas/)
* [Chamilo LMS](https://chamilo.org/en/2019/06/13/new-features-in-chamilo-1-11-10/)
* [Dokeos Community Edition](https://www.dokeos.com/dokeos-community-edition/)
* [Moodle LMS](https://moodle.org/?lang=pt)
* [Opigno LMS](https://www.opigno.org/en)
* [Sakai LMS](https://www.sakailms.org/)

### Análise estatística de dados massivos

* [Learning Locker](https://www.ht2labs.com/learning-locker-community/features/) - Sistema que permite gestão estatística de dados de aprendizagem de grande volume
 
### Videoconferência e Comunicação de Grupo

* [Big Blue Button](https://bigbluebutton.org) - Sistema de Web Conferencing para ensino à distância
* [Jitsi](https://jitsi.org) - Sistema de Web Coonferencing para ensino à distância
* [Mattermost](https://mattermost.com/) - Comunicação por escrito para equipas e comunidades
* [Mumble](https://www.mumble.com/) - Comunicação áudio de alta qualidade e baixa latência
* [Element](https://element.io/features) - Comunicação em grupo na Web verdadeiramente privada e descentralizada (texto, voz e video)
* [Rocket.chat](https://rocket.chat/) - Comunicação por escrito para equipas e comunidades (pode incluir o Jitsi para video conferência)
* [UnHangout](https://www.youtube.com/watch?v=vYxmd3BmOW0) - Platforma para criar eventos de larga escala geridos pelos participantes (UnConference, World Caffe, etc). 

### Gestão de escolas e alunos, intranet e colaboração Online

* [Nextcloud Hub](https://nextcloud.com/hub/) - Gestão de documentos na cloud (pode incluir Office Online, videoconferência, etc.)
* [OpenLucius]() - Sistema que permite criar uma intranet
* [OpenSocial](https://www.getopensocial.com/) - Sistema que permite criar uma rede social 
* [Omeka](https://omeka.org/) - Criação de exibições e colecções virtuais
* [Collabora Online](https://nextcloud.com/collaboraonline/) - Suite de Escritório na web
* [Etherpad](https://etherpad.org/) - Editor de texto colaborativo
* [Fedena ERP](https://projectfedena.org/) - Sistema de Gestão Escolar

### Gestão de Conteúdos (websites, blogues, etc)

* [AsgardCMS](https://asgardcms.com/features) - Sistema de gestão de conteúdos modular
* [Drupal](https://www.drupal.org) - Sistema de gestão de conteúdos para criar e manter sites, blogs ou aplicações, com algumas distribuições direccionadas para ensino, etc 
* [Grav](https://getgrav.org) - Sistema de gestão de conteúdos para criar e manter sites, blogs ou aplicações, extensível e sem necessidade de ter uma base de dados
* [Lavalite](http://www.lavalite.org/) - Sistema de gestão de conteúdo e criação de plataformas que requer algum conhecimento de programação
* [OctoberCMS](https://octobercms.com/) - Sistema de gestão de conteúdos simples de usar
* [PyroCMS](https://pyrocms.com/) - Sistema de gestão de conteúdos extensível que requer conhecimentos de programação
* [WordPress](https://pt.wordpress.org/) - Sistema de gestão de conteúdos para criar e manter sites, blogs ou aplicações

### Criação de redes privadas 

* [OpenVPN](https://openvpn.net/) - Criação de uma rede privada VPN
* [OpenVPN GUI](https://github.com/OpenVPN/openvpn-gui/) - Sistema gráfico de gestão de redes OpenVPN para Microsoft Windows

## A ser instalado pelos utilizadores

### Processamento de texto, bibliografias, etc

* [Calibre](https://calibre-ebook.com/) - Organização, edição, e catalogação de ebooks
* [Ghostwriter](https://wereturtle.github.io/ghostwriter/) - Editor de markdown
* [Joplin](https://joplinapp.org/) - Tirar notas, com suporte de markdown e sincronização similar ao Evernote
* [LibreOffice](https://www.libreoffice.org/) - Suite de escritório (editor de texto, editor de folhas de cálculo, etc.)
* [Okular](https://okular.kde.org/) - Leitor e anotador de pdf e outros formatos
* [TextGrid](https://textgrid.de/en/) - Análise e edição de texto
* [Zotero](https://www.zotero.org/) - Gestão de bibliografias

### Música

* [MuseScore](https://musescore.org) - Escrita e leitura de pautas musicais
* [Solfege](https://www.gnu.org/software/solfege/) - Treino de ouvido e música

### Matemática e Ciência

* [FisicaLab](https://www.gnu.org/software/fisicalab/) - Resolução de problemas de Física
* [Geogebra](https://www.geogebra.org/) - Aplicação de geometria, álgebra, estatística e cálculo
* [Mathics](https://mathics.github.io/) - Álgebra
* [Octave](https://www.gnu.org/software/octave/) - Linguagem semelhante ao MATLAB para computação numérica
* [Scilab e Xcos](https://www.scilab.org/) - Sistema similar ao MATLAB e Simulink
* [Tracker](https://physlets.org/tracker/) – Análise e modelação física de vídeos

### Gráficos e CAD

* [Gephi](https://gephi.org/) - Grafos
* [Palladio](https://hdlab.stanford.edu/palladio/) - Criação de visualizações de dados históricos complexos
* [TimelineJS](https://timeline.knightlab.com/) - Criação de timelines
* [QGIS](https://qgis.org/pt_PT/site/) - Programa SIG/GIS completo para criação, edição e análise de Informação Geográfica
* [FreeCAD](https://www.freecadweb.org/?lang=pt_PT) - Programa livre de desenho 3D paramétrico, aplicável a modelação.
* [LibreCAD](https://librecad.org/) - Programa livre de desenho CAD 2D, para desenho técnico, topografia, etc 2D, equivalente ao AutoCAD.
* [SweetHome3D](http://www.sweethome3d.com/pt/) - Programa livre de desenho de arquitetura e interiores em 3D, equivalente a soluções BIM.

### Imagem

* [Blender](https://www.blender.org) - Modelação e Animação 3D
* [Darktable](https://darktable.org) - Edição e organização de fotografias
* [Digikam](https://www.digikam.org) - Edição e organização de fotografias
* [GIMP](https://www.gimp.org) - Edição de imagem bitmap
* [Hugin](http://hugin.sourceforge.net/) - Ferramenta para criar imagens em panorama
* [Inkscape](https://inkscape.org) - Desenho vectorial
* [Krita](https://krita.org) - Pintura digital
* [MyPaint](http://mypaint.org) - Pintura digital
* [OpenToonz](https://opentoonz.github.io/e/) - Animação 2D
* [Scribus](https://www.scribus.net/) - Edição gráfica (revistas, jornais, etc)
* [Synfig](https://www.synfig.org) - Animação 2D
* [Tropy](https://tropy.org) - Gestão de imagens
* [Tux Paint](http://www.tuxpaint.org) - Ferramenta de pintura para crianças

### Audio e Vídeo

* [Ardour](http://ardour.org) - Gravação, edição e mistura de áudio
* [Audacity](https://www.audacityteam.org) - Edição áudio
* [Kdenlive](https://kdenlive.org) - Editor de vídeo
* [Natron](https://natrongithub.github.io) - Composição e colorização de vídeo
* [OBS Studio](https://obsproject.com) - Estúdio de gravação e emissão (streaming) de aulas e palestras online
* [Olive](https://olivevideoeditor.org) - Editor de video
* [OpenShot](https://www.openshot.org/pt/) - Editor de vídeo
* [Toonloop](http://www.toonloop.com/) - Animação stop-motion

### Colaboração

* [Atom Teletype](https://github.com/atom/teletype) - Editor de texto com extensão para edição simultânea
* [Jamulus](http://llcon.sourceforge.net/) - Ensaios à distância em tempo real para músicos, sem latência.
* [Jitsi Meet](https://meet.jit.si) - Video conferência no browser
* [KDE Connect](https://kdeconnect.kde.org) - Controlar o computador com o telemóvel em sistemas Linux KDE
* [Mattermost](https://mattermost.com/download/) - Comunicação por escrito para equipas e comunidades (correspondente aplicação desktop e mobile e via browser)
* [Mumble](https://www.mumble.com/) - Comunicação áudio de alta qualidade e baixa latência  (correspondente aplicação desktop e mobile e via browser)
* [Rocket.chat](https://rocket.chat/install) - Comunicação por escrito para equipas e comunidades  (correspondente aplicação desktop e mobile e via browser)
* [Element](https://element.io/get-started) - Comunicação em grupo na Web  (correspondente aplicação desktop e mobile e via browser)

### Outros

* [Framalibre](https://framasoft.org/en/) - Conjunto de ferramentas e recursos
* [GCompris](https://gcompris.net) - Actividades para crianças dos 2 aos 10 anos
 
* [Software Livre para Educação da Universidade Federal do Rio Grande do Sul, Brasil](https://www.ufrgs.br/soft-livre-edu/wiki/Tabela_Din%C3%A2mica_Software_Educacional_livre_-_Portugu%C3%AAs_Europeu) - Esta tabela procura reunir programas livres que abordem conteúdos curriculares escolares.
